= Go

Go is a compiled language, statically typed, created by Google for its own internal usage with highly distributed systems.

== https://github.com/cpmech/gosl[Gosl]

== https://github.com/gonum/gonum[Gonum]

== Installation

If you do not have already installed Go, please refer to the official site https://golang.org/doc/install.
