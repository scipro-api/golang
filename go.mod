module gitlab.com/scipro-api/golang

go 1.12

require (
	github.com/opennota/check v0.0.0-20180911053232-0c771f5545ff // indirect
	golang.org/x/tools v0.0.0-20200506181757-c20a87c16a39 // indirect
)
